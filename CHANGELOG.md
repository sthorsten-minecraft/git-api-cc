# Changelog

Current Version: *v1.0 (release)*

## Version 1.0 (release)

- Added Error Handling
- Added Terminal Output (can be turned on/off)
- Restructured the program

## Version 0.2 (alpha)

- Changed API to be Object-Oriented

## Version 0.1 (alpha)

- First Release
- Added basic functionality