# Git API

## Description

This is my Git API for ComputerCraft. It allows for easy cloning and updating files through git.

## Features

This API contains the following features:

- Download a complete (public) repository from Github *or* Gitlab
- The branch of the selected repository can be selected

## Example Usage

Download a repository from Github:

*Step 1:* Load API
```lua
Git = assert(loadfile("git.lua")())
```

*Step 1.5 (**optional**):* Enable / Disable (Debug) Output
```lua
Git:showOutput(boolean)
-- boolean: true or false - Enables or Disables printing the output to the terminal
```

*Step 2:* Select the Hosting Provider
```lua
g1 = Git:setProvider(provider)
-- provider: Must be "gitlab" or "github"
```

*Step 3:* Select the repository
```lua
Git:setRepository(repositoryPrefix, repositoryName, repositoryBranch)
-- repositoryPrefix: The namespace of the repository (usually a username or groupname)
-- repositoryName: The name of the repository
-- repositoryBranch: The branch of the repository that will be downloaded
```

*Step 4:* Clone the repository
```lua
Git:cloneTo(folder)
-- folder: The local folder the repository files will be placed in
```


## Changelog

Current Version: v1.0 (release)

*Latest Changes:*

- Added Error Handling
- Added Terminal Output (can be turned on/off)
- Restructured the program

The full changelog can be found [here](CHANGELOG.md).

## License

The MIT License

See the [LICENSE](LICENSE) file

(C) 2018 Thorsten Schmitt

## 


